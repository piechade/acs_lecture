﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CrossSum
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_sum_Click(object sender, RoutedEventArgs e)
        {
            Calc();
        }

        private void TxB_sum_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Down)
            {
                Calc();
            }
        }

        private void TxB_sum_TextChanged(object sender, TextChangedEventArgs e)
        {
            tB_result.Text = "";
        }
        private void Calc()
        {
            int max = 6;
            if (Regex.IsMatch(txB_sum.Text, @"^\d+$"))
            {
                if (txB_sum.Text.Length == max)
                {
                    tB_result.Text = txB_sum.Text.Select(digit => int.Parse(digit.ToString())).Sum().ToString();
                }
                else
                {
                    tB_result.Text = "Error! Onyl digits are allowed!";
                }
            }
            else
            {
                tB_result.Text = String.Format("Pleas enter {0} digits!", max);
            }
        }
    }
}
