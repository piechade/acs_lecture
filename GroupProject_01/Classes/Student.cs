﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GroupProject_01
{
    public class Student
    {
        public string MatNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Group { get; set; }
        public string City { get; set; }
        public string PlzCode { get; set; }
        public DateTime BirthDate { get; set; }
        public string Gender { get; set; }
        public string Language { get; set; }
        public string Hobies { get; set; }
        public string Email { get; set; }
    }
}
