﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace GroupProject_01
{
    public class MatNo2Image : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            BitmapImage image = new BitmapImage();
            string file = @"\Images\" + value + ".jpeg";
            try
            {
                var path = Environment.CurrentDirectory + file;
                image = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));
            }
            catch (Exception)
            {

                var path = Environment.CurrentDirectory + @"\Images\owl.png"; ;
                image = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));
            }
            return image;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
