﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace GroupProject_01
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static List<Student> _students;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            _students = MyStorage.ReadXML<List<Student>>("students.xml");
            if (_students == null)
                _students = new List<Student>();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            //Student s1 = new Student
            //{
            //    MatNo = "mat",
            //    FirstName = "fn",
            //    LastName = "ln",
            //    BirthDate = DateTime.Today,
            //    Gender = "m",
            //    City = "hd",
            //    PlzCode = "69126",
            //    Group = "123",
            //    Hobies = "hob",
            //    Language = "de, en"
            //};
            //Student s2 = new Student
            //{
            //    MatNo = "mat",
            //    FirstName = "fn",
            //    LastName = "ln",
            //    BirthDate = DateTime.Today,
            //    Gender = "m",
            //    City = "hd",
            //    PlzCode = "69126",
            //    Group = "123",
            //    Hobies = "hob",
            //    Language = "de, en"
            //};
            //Student s3 = new Student
            //{
            //    MatNo = "mat",
            //    FirstName = "fn",
            //    LastName = "ln",
            //    BirthDate = DateTime.Today,
            //    Gender = "m",
            //    City = "hd",
            //    PlzCode = "69126",
            //    Group = "123",
            //    Hobies = "hob",
            //    Language = "de, en"
            //};

            //_students = new List<Student>
            //{
            //    s1,
            //    s2,
            //    s3
            //};

            MyStorage.StorageXML<List<Student>>("students.xml", _students);
        }
    }
}
