﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GroupProject_01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Lbx_students.ItemsSource = App._students;
        }

        private void TBx_filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            Lbx_students.ItemsSource = App._students.Where(x =>
                x.FirstName.StartsWith(TBx_filter.Text, StringComparison.InvariantCultureIgnoreCase) ||
                x.LastName.StartsWith(TBx_filter.Text, StringComparison.InvariantCultureIgnoreCase)
            );
        }
    }
}
